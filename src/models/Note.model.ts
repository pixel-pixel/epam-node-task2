import { model, Schema } from "mongoose";
import { Note } from "src/common/types/Note.type";

const NoteSchema = new Schema<Note>({
  userId: {type: String, require: true},
  completed: {type: Boolean, require: true},
  text: {type: String, require: true},
  createdDate: {type: String, require: true},
})

export default model<Note>('Note', NoteSchema)