import { Request, Response } from "express"
import { User } from "../common/types/User.type"
import NoteModel from "../models/Note.model"
import getFormatedDateTool from "../tools/getFormatedDate.tool"

class NotesController {
  async getUserNotes(req: Request, res: Response) {
    const { _id: userId } = req['user'] as User
    const notes = await NoteModel.find({userId}) || []

    return res.json({
      "offset": 0,
      "limit": 0,
      "count": 0,
      notes
    })
  }

  async addNoteForUser(req: Request, res: Response) {
    const { _id: userId } = req['user'] as User
    const { text } = req.body
    const note = new NoteModel({userId, text, completed: false, createdDate: getFormatedDateTool()})
    await note.save()

    return res.json({"message": "Success"})
  }

  async getUserNoteById(req: Request, res: Response) {
    const { id: noteId } = req.params
    const note = await NoteModel.findById(noteId) || null

    return res.json({note})
  }

  async updateUserNoteById(req: Request, res: Response) {
    const { id: noteId } = req.params
    const { text } = req.body
    const note = await NoteModel.findById(noteId)
    note.text = text

    await note.save()

    return res.json({"message": "Success"})
  }

  async checkUserNoteById(req: Request, res: Response) {
    const { id: noteId } = req.params
    const { text } = req.body
    const note = await NoteModel.findById(noteId)
    note.completed = !note.completed

    await note.save()

    return res.json({"message": "Success"})
  }

  async deleteUserNoteById(req: Request, res: Response) {
    const { id: noteId } = req.params
    const { text } = req.body
    const note = await NoteModel.findByIdAndRemove(noteId)
    note.text = text

    return res.json({"message": "Success"})
  }
}

export default new NotesController