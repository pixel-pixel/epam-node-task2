import { Router } from "express"
import notesController from "../controllers/notes.controller"
import authController from "../controllers/auth.controller"

const notesRouter = Router()

notesRouter.get('/', notesController.getUserNotes)
notesRouter.post('/', notesController.addNoteForUser)
notesRouter.get('/:id', notesController.getUserNoteById)
notesRouter.put('/:id', notesController.updateUserNoteById)
notesRouter.patch('/:id', notesController.checkUserNoteById)
notesRouter.delete('/:id', notesController.deleteUserNoteById)

export default notesRouter