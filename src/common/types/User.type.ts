import Roles from "../enums/Roles.enum";

type User = {
    _id: string
    username: string
    password: string
}

export type { User }