import Joi from "joi";
import Roles from "../enums/Roles.enum";

const registerSchema = Joi.object({
  username: Joi.string().required(),
  password: Joi.string().required(),
})

export default registerSchema