import Joi from "joi"

const patchPasswordSchema = Joi.object({
  oldPassword: Joi.string().required(),
  newPassword: Joi.string().required(),
})

export default patchPasswordSchema