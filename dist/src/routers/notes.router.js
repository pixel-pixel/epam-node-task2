"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const notes_controller_1 = __importDefault(require("../controllers/notes.controller"));
const notesRouter = (0, express_1.Router)();
notesRouter.get('/', notes_controller_1.default.getUserNotes);
notesRouter.post('/', notes_controller_1.default.addNoteForUser);
notesRouter.get('/:id', notes_controller_1.default.getUserNoteById);
notesRouter.put('/:id', notes_controller_1.default.updateUserNoteById);
notesRouter.patch('/:id', notes_controller_1.default.checkUserNoteById);
notesRouter.delete('/:id', notes_controller_1.default.deleteUserNoteById);
exports.default = notesRouter;
