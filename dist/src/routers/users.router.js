"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const users_controller_1 = __importDefault(require("../controllers/users.controller"));
const usersRouter = (0, express_1.Router)();
usersRouter.get('/me', users_controller_1.default.getMe);
usersRouter.delete('/me', users_controller_1.default.deleteMe);
usersRouter.patch('/me', users_controller_1.default.patchMePassword);
exports.default = usersRouter;
