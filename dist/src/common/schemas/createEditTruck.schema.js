"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("joi"));
const TruckStatus_enum_1 = __importDefault(require("../enums/TruckStatus.enum"));
const TruckTypes_enum_1 = __importDefault(require("../enums/TruckTypes.enum"));
const sreateEditTruckSchema = joi_1.default.object({
    type: joi_1.default.string().valid(TruckTypes_enum_1.default.SPRINTER, TruckTypes_enum_1.default.SMALL_STRAIGHT, TruckTypes_enum_1.default.LARGE_STRAIGHT).required(),
    status: joi_1.default.string().valid(TruckStatus_enum_1.default.OL, TruckStatus_enum_1.default.IS)
});
exports.default = sreateEditTruckSchema;
