"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TruckStatus;
(function (TruckStatus) {
    TruckStatus["OL"] = "OL";
    TruckStatus["IS"] = "IS";
})(TruckStatus || (TruckStatus = {}));
exports.default = TruckStatus;
