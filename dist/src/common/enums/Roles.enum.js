"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Roles;
(function (Roles) {
    Roles["Driver"] = "DRIVER";
    Roles["Shipper"] = "SHIPPER";
})(Roles || (Roles = {}));
exports.default = Roles;
