"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getSizes = void 0;
var TruckTypes;
(function (TruckTypes) {
    TruckTypes["SPRINTER"] = "SPRINTER";
    TruckTypes["SMALL_STRAIGHT"] = "SMALL_STRAIGHT";
    TruckTypes["LARGE_STRAIGHT"] = "LARGE_STRAIGHT,";
})(TruckTypes || (TruckTypes = {}));
const getSizes = (t) => {
    switch (t) {
        case 'SPRINTER': return {
            width: 300,
            length: 250,
            height: 170,
            payload: 1700
        };
        case 'SMALL_STRAIGHT': return {
            width: 500,
            length: 250,
            heigth: 170,
            payload: 2500
        };
        default: return {
            width: 700,
            length: 350,
            height: 200,
            payload: 4000
        };
    }
};
exports.getSizes = getSizes;
exports.default = TruckTypes;
