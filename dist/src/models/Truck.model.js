"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const truckShema = new mongoose_1.Schema({
    created_by: { type: String, required: true },
    assigned_to: { type: String },
    type: { type: String, required: true },
    status: { type: String, require: true },
    created_date: { type: String, require: true }
});
exports.default = (0, mongoose_1.model)('Truck', truckShema);
