"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const NoteSchema = new mongoose_1.Schema({
    userId: { type: String, require: true },
    completed: { type: Boolean, require: true },
    text: { type: String, require: true },
    createdDate: { type: String, require: true },
});
exports.default = (0, mongoose_1.model)('Note', NoteSchema);
