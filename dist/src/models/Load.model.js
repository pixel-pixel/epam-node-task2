"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importStar(require("joi"));
const mongoose_1 = require("mongoose");
const LoadState_enum_1 = __importDefault(require("../common/enums/LoadState.enum"));
const LoadStatus_enum_1 = __importDefault(require("../common/enums/LoadStatus.enum"));
const loadShema = new mongoose_1.Schema({
    created_by: { type: String, require: true },
    assigned_to: { type: String },
    status: { type: String, enum: [
            LoadStatus_enum_1.default.NEW,
            LoadStatus_enum_1.default.POSTED,
            LoadStatus_enum_1.default.ASSIGNED,
            LoadStatus_enum_1.default.SHIPPED
        ], default: 'NEW' },
    state: { type: LoadState_enum_1.default, default: null },
    name: { type: String, require: true },
    payload: { type: Number, require: true },
    pickup_address: { type: String, require: true },
    delivery_address: { type: String, require: true },
    dimensions: joi_1.default.object({
        width: joi_1.string,
        length: joi_1.string,
        height: joi_1.string
    }).required(),
    logs: { type: Object },
    created_date: { type: String }
});
exports.default = (0, mongoose_1.model)('Load', loadShema);
