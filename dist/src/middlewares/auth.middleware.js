"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv = __importStar(require("dotenv"));
const jwt = __importStar(require("jsonwebtoken"));
const User_model_1 = __importDefault(require("../models/User.model"));
dotenv.config();
const authMiddleware = (role = null) => (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    if (req.method === 'OPTIONS')
        next();
    try {
        const arr = (_a = req.headers.authorization) === null || _a === void 0 ? void 0 : _a.split(' ');
        if (!arr)
            throw Error('authorization error');
        const token = arr[arr.length - 1];
        const tokenData = jwt.verify(token, process.env.JWT_SECRET_KEY);
        if (!tokenData)
            throw Error('authorization error');
        const user = yield User_model_1.default.findOne({ _id: tokenData.id });
        if (!user)
            throw Error('user doesn`t exist');
        req['user'] = user;
        next();
    }
    catch (e) {
        const message = e.message;
        res.status(400).json({ message });
    }
});
exports.default = authMiddleware;
