"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const User_model_1 = __importDefault(require("../models/User.model"));
const joi_1 = __importDefault(require("joi"));
const patchPassword_schema_1 = __importDefault(require("../common/schemas/patchPassword.schema"));
const bcryptjs_1 = __importDefault(require("bcryptjs"));
class UsersController {
    getMe(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const user = req['user'];
                res.json({ user });
            }
            catch (e) {
                const message = e.message;
                res.status(400).json({ message });
            }
        });
    }
    deleteMe(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { _id } = req['user'];
                yield User_model_1.default.deleteOne({ _id });
                res.json({ message: 'Profile deleted successfully' });
            }
            catch (e) {
                const message = e.message;
                res.status(400).json({ message });
            }
        });
    }
    patchMePassword(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { oldPassword, newPassword } = joi_1.default.attempt(req.body, patchPassword_schema_1.default);
                const { _id, password } = req['user'];
                const validPassword = bcryptjs_1.default.compareSync(oldPassword, password);
                if (!validPassword)
                    throw Error('bad password');
                const cryptedPassword = bcryptjs_1.default.hashSync(newPassword, 7);
                yield User_model_1.default.updateOne({ _id }, { password: cryptedPassword });
                res.json({ message: 'Password changed successfully' });
            }
            catch (e) {
                const message = e.message;
                res.status(400).json({ message });
            }
        });
    }
}
exports.default = new UsersController;
