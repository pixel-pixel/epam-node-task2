"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Note_model_1 = __importDefault(require("../models/Note.model"));
const getFormatedDate_tool_1 = __importDefault(require("../tools/getFormatedDate.tool"));
class NotesController {
    getUserNotes(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { _id: userId } = req['user'];
            const notes = (yield Note_model_1.default.find({ userId })) || [];
            return res.json({
                "offset": 0,
                "limit": 0,
                "count": 0,
                notes
            });
        });
    }
    addNoteForUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { _id: userId } = req['user'];
            const { text } = req.body;
            const note = new Note_model_1.default({ userId, text, completed: false, createdDate: (0, getFormatedDate_tool_1.default)() });
            yield note.save();
            return res.json({ "message": "Success" });
        });
    }
    getUserNoteById(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id: noteId } = req.params;
            const note = (yield Note_model_1.default.findById(noteId)) || null;
            return res.json({ note });
        });
    }
    updateUserNoteById(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id: noteId } = req.params;
            const { text } = req.body;
            const note = yield Note_model_1.default.findById(noteId);
            note.text = text;
            yield note.save();
            return res.json({ "message": "Success" });
        });
    }
    checkUserNoteById(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id: noteId } = req.params;
            const { text } = req.body;
            const note = yield Note_model_1.default.findById(noteId);
            note.completed = !note.completed;
            yield note.save();
            return res.json({ "message": "Success" });
        });
    }
    deleteUserNoteById(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id: noteId } = req.params;
            const { text } = req.body;
            const note = yield Note_model_1.default.findByIdAndRemove(noteId);
            note.text = text;
            return res.json({ "message": "Success" });
        });
    }
}
exports.default = new NotesController;
