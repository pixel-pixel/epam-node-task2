"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const User_model_1 = __importDefault(require("../models/User.model"));
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const joi_1 = __importDefault(require("joi"));
const register_schema_1 = __importDefault(require("../common/schemas/register.schema"));
const login_schema_1 = __importDefault(require("../common/schemas/login.schema"));
const token_tool_1 = __importDefault(require("../tools/token.tool"));
class AuthController {
    register(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const body = joi_1.default.attempt(req.body, register_schema_1.default, 'invalid method`s body');
                const { username, password } = body;
                const user = yield User_model_1.default.findOne({ username });
                if (user)
                    throw Error('user with this email already exist');
                const hashPassword = bcryptjs_1.default.hashSync(password, 7);
                const newUser = new User_model_1.default({
                    username,
                    password: hashPassword,
                });
                yield newUser.save();
                return res.json({ message: "Profile created successfully" });
            }
            catch (e) {
                const message = e.message;
                res.status(400).json({ message });
            }
        });
    }
    login(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const body = joi_1.default.attempt(req.body, login_schema_1.default);
                const { username, password } = body;
                const user = yield User_model_1.default.findOne({ username });
                if (!user)
                    throw Error('user with this email don`t exist');
                const validPassword = bcryptjs_1.default.compareSync(password, user.password);
                if (!validPassword)
                    throw Error('bad password');
                const jwt_token = (0, token_tool_1.default)(user._id, "kek");
                res.json({ message: "Success", jwt_token });
            }
            catch (e) {
                const message = e.message;
                res.status(400).json({ message });
            }
        });
    }
}
exports.default = new AuthController;
