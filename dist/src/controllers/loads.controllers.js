"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("joi"));
const LoadStatus_enum_1 = __importDefault(require("../common/enums/LoadStatus.enum"));
const TruckStatus_enum_1 = __importDefault(require("../common/enums/TruckStatus.enum"));
const Truck_model_1 = __importDefault(require("../models/Truck.model"));
const addLoadForUser_shema_1 = __importDefault(require("../common/schemas/addLoadForUser.shema"));
const Load_model_1 = __importDefault(require("../models/Load.model"));
const TruckTypes_enum_1 = require("../common/enums/TruckTypes.enum");
const getFormatedDate_tool_1 = __importDefault(require("../tools/getFormatedDate.tool"));
const LoadState_enum_1 = __importDefault(require("../common/enums/LoadState.enum"));
class LoadsController {
    getUserLoads(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    addLoadForUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const body = joi_1.default.attempt(req.body, addLoadForUser_shema_1.default);
                const { _id: userId } = req['user'];
                const load = new Load_model_1.default(Object.assign(Object.assign({}, body), { created_by: userId, created_date: (0, getFormatedDate_tool_1.default)() }));
                yield load.save();
                return res.json({ message: 'Load created successfully' });
            }
            catch (e) {
                const message = e.message;
                res.status(400).json({ message });
            }
        });
    }
    getUserActiveLoad(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { _id: userId } = req['user'];
                const truck = yield Truck_model_1.default.findOne({ assigned_to: userId });
                if (!truck || truck.status == TruckStatus_enum_1.default.IS)
                    return res.json({ load: null });
                const load = yield Load_model_1.default.findOne({ assigned_to: truck._id });
                return res.json({ load });
            }
            catch (e) {
                const message = e.message;
                res.status(400).json({ message });
            }
        });
    }
    nextLoadState(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { _id: userId } = req['user'];
                const truck = yield Truck_model_1.default.findOne({ assigned_to: userId });
                if (!truck)
                    throw Error('you don`t assign to truck');
                const load = yield Load_model_1.default.findOne({ assigned_to: truck._id });
                if (!load)
                    throw Error('truck don`t assign to load');
                switch (load.state) {
                    case LoadState_enum_1.default.ROUTE_TO_PICK_UP:
                        load.state = LoadState_enum_1.default.ARIVED_TO_PICK_UP;
                        break;
                    case LoadState_enum_1.default.ARIVED_TO_PICK_UP:
                        load.state = LoadState_enum_1.default.ROUTE_TO_DELIVERY;
                        break;
                    default:
                        load.state = LoadState_enum_1.default.ARIVED_TO_DELIVERY;
                        load.status = LoadStatus_enum_1.default.SHIPPED;
                        truck.status = TruckStatus_enum_1.default.IS;
                        yield truck.save();
                }
                yield load.save();
                return res.json({ message: `Load state changed to '${load.state}'` });
            }
            catch (e) {
                const message = e.message;
                res.status(400).json({ message });
            }
        });
    }
    getUserLoadById(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { _id: userId } = req['user'];
                const { id: loadId } = req.params;
                const load = yield Load_model_1.default.findById(loadId);
                const { _id: truckId } = yield Truck_model_1.default.findOne({ assigned_to: userId });
                console.log(userId + ' ' + load.assigned_to + ' ' + load.created_by);
                if (load.assigned_to != truckId && load.created_by != userId) {
                    throw Error('you don`t have permissions');
                }
                return res.json({ load });
            }
            catch (e) {
                const message = e.message;
                res.status(400).json({ message });
            }
        });
    }
    updateUserLoadById(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const body = joi_1.default.attempt(req.body, addLoadForUser_shema_1.default);
                const { _id: userId } = req['user'];
                const { id: loadId } = req.params;
                let load = yield Load_model_1.default.findById(loadId);
                if (load.created_by != userId)
                    throw Error('you don`t have permissin');
                load = Object.assign(load, body);
                load.save();
                return res.json({ message: 'Load details changed successfully' });
            }
            catch (e) {
                const message = e.message;
                res.status(400).json({ message });
            }
        });
    }
    deleteUserLoadById(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { _id: userId } = req['user'];
                const { id: loadId } = req.params;
                const load = yield Load_model_1.default.findById(loadId);
                if (load.created_by != userId)
                    throw Error('you don`t have permissions');
                yield load.delete();
                return res.json({ message: 'Load deleted successfully' });
            }
            catch (e) {
                const message = e.message;
                res.status(400).json({ message });
            }
        });
    }
    postUserLoadById(req, res) {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { _id: userId } = req['user'];
                const loadId = req.params.id;
                const load = yield Load_model_1.default.findById(loadId);
                if (load.created_by != userId)
                    throw Error('you don`t have permissions');
                load.status = LoadStatus_enum_1.default.POSTED;
                yield load.save();
                const trucks = yield Truck_model_1.default.find()
                    .where('status').equals(TruckStatus_enum_1.default.IS)
                    .where('assigned_to').ne(null);
                const findedTruck = trucks.find(t => {
                    const sizes = (0, TruckTypes_enum_1.getSizes)(t.type);
                    return sizes.payload >= load.payload &&
                        sizes.width >= load.dimensions.width &&
                        sizes.length >= load.dimensions.length &&
                        sizes.height >= load.dimensions.height;
                });
                if (findedTruck) {
                    findedTruck.status = TruckStatus_enum_1.default.OL;
                    yield findedTruck.save();
                    load.status = LoadStatus_enum_1.default.ASSIGNED;
                    load.assigned_to = findedTruck._id;
                    load.state = LoadState_enum_1.default.ROUTE_TO_PICK_UP;
                    load.logs = (_a = load.logs) !== null && _a !== void 0 ? _a : [];
                    load.logs.push({
                        message: `truck found, id: ${findedTruck._id}`,
                        time: (0, getFormatedDate_tool_1.default)()
                    });
                    yield load.save();
                    return res.json({
                        message: 'Load posted successfully',
                        driver_found: true
                    });
                }
                else {
                    load.logs = (_b = load.logs) !== null && _b !== void 0 ? _b : [];
                    load.logs.push({
                        message: 'truck not found',
                        time: (0, getFormatedDate_tool_1.default)()
                    });
                    yield load.save();
                    return res.json({
                        message: 'Load can`t be posted. Driver not found',
                        driver_found: false
                    });
                }
            }
            catch (e) {
                const message = e.message;
                res.status(400).json({ message });
            }
        });
    }
    getUserLoadShippingInfoById(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { _id: userId } = req['user'];
                const { id: loadId } = req.params;
                const load = yield Load_model_1.default.findById(loadId);
                if (load.created_by != userId)
                    throw Error('you don`t have permission');
                const truck = yield Truck_model_1.default.findById(load.assigned_to);
                return res.json({ load, truck });
            }
            catch (e) {
                const message = e.message;
                res.status(400).json({ message });
            }
        });
    }
}
exports.default = new LoadsController;
