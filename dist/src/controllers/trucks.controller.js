"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("joi"));
const Truck_model_1 = __importDefault(require("../models/Truck.model"));
const createEditTruck_schema_1 = __importDefault(require("../common/schemas/createEditTruck.schema"));
const getFormatedDate_tool_1 = __importDefault(require("../tools/getFormatedDate.tool"));
class TrucksController {
    getUsersTrucks(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { _id } = req['user'];
                const trucks = yield Truck_model_1.default.find({ created_by: _id });
                return res.json({ trucks });
            }
            catch (e) {
                const message = e.message;
                res.status(400).json({ message });
            }
        });
    }
    addUsersTruck(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { _id: userId } = req['user'];
                const { type, status = 'IS' } = joi_1.default.attempt(req.body, createEditTruck_schema_1.default);
                const truck = new Truck_model_1.default({
                    created_by: userId,
                    type,
                    status,
                    created_date: (0, getFormatedDate_tool_1.default)()
                });
                yield truck.save();
                return res.json({ message: 'Truck created successfully' });
            }
            catch (e) {
                const message = e.message;
                res.status(400).json({ message });
            }
        });
    }
    getUsersTruckById(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { _id } = req['user'];
                const truck = yield Truck_model_1.default.findById(req.params.id);
                if (truck.created_by != _id)
                    throw Error('you don`t have permissions');
                return res.json({ truck });
            }
            catch (e) {
                const message = e.message;
                res.status(400).json({ message });
            }
        });
    }
    editUsersTruckById(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { _id: userId } = req['user'];
                const { id: truckId } = req.params;
                const { type, status = 'OL' } = joi_1.default.attempt(req.body, createEditTruck_schema_1.default);
                const truck = yield Truck_model_1.default.findById(truckId);
                if (truck.created_by != userId)
                    throw Error('you don`t have permissions');
                truck.type = type;
                truck.status = status;
                yield truck.save();
                return res.json({ message: 'Truck assigned successfully' });
            }
            catch (e) {
                const message = e.message;
                res.status(400).json({ message });
            }
        });
    }
    deleteUsersTruckById(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { _id: userId } = req['user'];
                const { id: trackId } = req.params;
                const truck = yield Truck_model_1.default.findById(trackId);
                if (truck.created_by != userId)
                    throw Error('you don`t have permissions');
                yield truck.delete();
                return res.json({ message: 'Truck deleted successfully' });
            }
            catch (e) {
                const message = e.message;
                res.status(400).json({ message });
            }
        });
    }
    asignTruckToUserById(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { _id: userId } = req['user'];
                const { id: trackId } = req.params;
                const assignedTrucks = yield Truck_model_1.default.find({ assigned_to: userId });
                if (assignedTrucks.length > 0)
                    throw Error('you already assigned to another truck');
                const truck = yield Truck_model_1.default.findById(trackId);
                truck.assigned_to = userId;
                yield truck.save();
                return res.json({ message: 'Truck assigned successfully' });
            }
            catch (e) {
                const message = e.message;
                res.status(400).json({ message });
            }
        });
    }
}
exports.default = new TrucksController;
